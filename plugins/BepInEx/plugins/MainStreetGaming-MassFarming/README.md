# MassFarming
Xeio's mod to assist with large scale farming in Valheim.
All credits go to Xeio and contributors on Github: https://github.com/Xeio/MassFarming

## Features
While holding the configured hotkey (default Left Shift or Left Bumper on controller) and using the pickup action the player will automatically harvest all pickupable items of the same type in a radius around the target. This is mainly intended for assisting with farming, but also works on world spawn pickups like rocks/sticks/berries and beehives.

Additionally, when planting crops, if the hotkey is held will plant in a grid around the original target (5x5 default grid size).