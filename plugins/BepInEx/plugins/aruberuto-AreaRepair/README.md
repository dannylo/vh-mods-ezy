# AreaRepair

Repair all pieces within a sphere centered around the target build piece when using the hammer to repair. 

The repair area radius can be configured.

The repair info messages will indicate how many pieces were repaired, how many require a missing workstation, and how many do not need repair.

Uses ServerSync.