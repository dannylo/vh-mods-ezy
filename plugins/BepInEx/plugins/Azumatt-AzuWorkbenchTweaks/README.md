# Description

## Simple workbench tweaks. Pulled from the previous OdinsQOL


`Version checks with itself. If installed on the server, it will kick clients who do not have it installed.`

`This mod uses ServerSync, if installed on the server and all clients, it will sync all configs to client`

`This mod uses a file watcher. If the configuration file is not changed with BepInEx Configuration manager, but changed in the file directly on the server, upon file save, it will sync the changes to all clients.`


---

## Features

`1 - General`

Lock Configuration [Synced with Server]
* If on, the configuration is locked and can be changed by server admins only.
    * Default Value: On

Auto Repair [Synced with Server]
* Auto repair your things when interacting with a build station
  * Default Value: Off

`2 - WorkBench`

WorkBenchRange [Synced with Server]
* Range you can build from workbench in meters
    * Default Value: 20

WorkBenchRange (Playerbase size) [Synced with Server]
* Workbench PlayerBase radius, this is how far away enemies spawn
    * Default Value: 20

Change No Roof Behavior [Synced with Server]
* Turns off the need for a roof to be above the workbench. This will also remove the no exposure need.
    * Default Value: Off

WorkBench Extension [Synced with Server]
* Range for workbench extensions. Workbench range must be 5 or greater for this to take effect.
    * Default Value: 5



---

`Feel free to reach out to me on discord if you need manual download assistance.`


# Author Information

### Azumatt

`DISCORD:` Azumatt#2625

`STEAM:` https://steamcommunity.com/id/azumatt/

For Questions or Comments, find me in the Odin Plus Team Discord or in mine:

[![https://i.imgur.com/XXP6HCU.png](https://i.imgur.com/XXP6HCU.png)](https://discord.gg/Pb6bVMnFb2)
<a href="https://discord.gg/pdHgy6Bsng"><img src="https://i.imgur.com/Xlcbmm9.png" href="https://discord.gg/pdHgy6Bsng" width="175" height="175"></a>