| `Version` | `Update Notes`                                                                                                            |
|-----------|---------------------------------------------------------------------------------------------------------------------------|
| 1.0.4     | - Courtesy Update for Bog Witch.                                                                                          |
| 1.0.3     | - Courtesy Update for Valheim 0.217.46. Just bumping the version and updating the last updated date. Nothing to see here. |
| 1.0.2     | - Update for Valheim's latest changes.                                                                                    |
| 1.0.1     | - Merge AzuWorkbenchInventoryRepair into this mod. It bothered some people, so it's all in one now.                       |
| 1.0.0     | - Initial Release                                                                                                         |