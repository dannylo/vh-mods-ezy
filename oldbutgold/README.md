# Valheim server mods para o EzyServer! ⚔

> Conjunto de plugins e configs para instalar e jogar no EzyServer

## Extras

Inclui script que baixa e sincroniza todos os plugins (arquivos .dll) da pasta plugins.

## Dependências 🛠

É necessário ter o [steam](https://store.steampowered.com/) + [valheim](https://store.steampowered.com/app/892970/Valheim/) + [bepinex para valheim](https://valheim.thunderstore.io/package/denikson/BepInExPack_Valheim/) instalados!

## Instalação 👷

### Linux 🐧
1. Baixe o repositório, dentro da pasta desejada, com o comando:
```sh
git clone https://codeberg.org/dannylo/vh-mods-ezy
```

2. Remova as pastas `config` e `plugins` do BepInEx.
```sh
rm -Rf ~/.steam/steam/steamapps/common/Valheim/BepInEx/config
rm -Rf ~/.steam/steam/steamapps/common/Valheim/BepInEx/plugins
```

3. Faça o link simbólico das pastas do repositório dentro da pasta do BepInEx.
```sh
ln -s /caminho/para/o/repo/vh-mods-ezy/config ~/.steam/steam/steamapps/common/Valheim/BepInEx/
ln -s /caminho/para/o/repo/vh-mods-ezy/plugins ~/.steam/steam/steamapps/common/Valheim/BepInEx/
```

4. Inicie o jogo e seja feliz! 😎 

### Windows 🪟
1. Abra o seu navegador e acesse: https://codeberg.org/dannylo/vh-mods-ezy

2. Baixe o **zip** do repositório e descompacte onde desejar.

3. Apague as pastas `config` e `plugins` dentro do BepInEx.

4. Copie ou mova as pastas `config` e `plugins`, que vieram no **zip** recém descompactado, para a pasta do BepInEx.

5. Inicie o jogo e seja feliz! 😎

## Atualização 🔁

### Linux 🐧
1. Feito a instalação conforme orientado, para atualizar basta entrar na pasta do repositório e executar o comando:
```sh
git pull
```

2. Inicie o jogo e seja feliz! 😎


### Windows 🪟
1. Repita o procedimento de instalação do Windows.

## Extras 🛝

### Script de atualização de plugins 📄

> Este é um bash script voltado para acessar o repositório de cada plugin, baixá-los **paralelamente** e colocá-los na pasta **plugins**.

- É necessário ter o bash (comum em qualquer distribuição linux) e os pacotes `parallel`, `7zip` e `curl`.
- Acompanha o binário [gum](https://github.com/charmbracelet/gum) para deixar a interface um pouco mais interessante.

Para utilizar o script, basta executar o comando:
```sh
./atualiza_plugins.sh
```

#### Plugins que não são atualizáveis 😭
Por limitações de distribuição dos plugins, alguns não podem ser atualizados por não serem disponibilidados em um repositório git. São esses:
- [BetterUI](https://www.nexusmods.com/valheim/mods/189) - é de 2021 e n tem releases no repositório git.
- [EquipmentAndQuickSlots](https://www.nexusmods.com/valheim/mods/92) - não tem nos releases do git, apenas no nexusmods.
- [ItemStacks](https://www.nexusmods.com/valheim/mods/66) - Não me lembro de usarmos isso kkkkkk.
- [UsefullTrophies](https://www.nexusmods.com/valheim/mods/123) - De 2021 e não tem releases no repositório git.
- [QuickStack](https://www.nexusmods.com/valheim/mods/29) - Foi descontinuado. Agora deve-se usar o [Quick Stack - Store - Sort - Trash - Restock](https://www.nexusmods.com/valheim/mods/2094?tab=description)... que tb não tem release no git.
- [YamlDotNet](https://www.nexusmods.com/valheim/mods/1491) - de 2021 e não tem um release no git.
