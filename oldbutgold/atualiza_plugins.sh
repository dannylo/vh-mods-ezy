#!/usr/bin/env bash
#  ______           __   __            _
#  |  _  \          \ \ / /           (_)
#  | | | |__ _ _ __  \ V /  __ ___   ___  ___ _ __
#  | | | / _` | '_ \ /   \ / _` \ \ / / |/ _ \ '__|
#  | |/ / (_| | | | / /^\ \ (_| |\ V /| |  __/ |
#  |___/ \__,_|_| |_\/   \/\__,_| \_/ |_|\___|_|
#
#  Dan Xavier - https://danxavier.com.br
#

#############
# VARIÁVEIS #
#############
#Script dir
script_dir=$(dirname $0)
# Dependências
dependences="parallel 7zip curl rsync"
# Gum PATH
gum=$script_dir/gum
gum_loading="$gum spin -s jump --title "Executando..." --spinner.foreground 2 --title.foreground 2 --show-output"
gum_installing="sudo $gum spin -s dot --title "Instalando..." --spinner.foreground 2 --title.foreground 2"
columns=$(tput cols)
if [[ $columns -gt 60 ]]; then
	width_text=60
	margin_text=$((($columns - $width_text - 2) / 2))
else
	width_text=$(($columns - 6))
	margin_text=2
fi

plugins="XPortal Jotunn MassFarming ValheimPlus"
#MultiCraft NoSmokeSimplified System.Data System.Runtime.Serialization TrashItems UnrestrictedPortals Valheim.DisplayBepInExInfo ValheimRecycle "

#NAO TEM COMO
#BetterUI EquipmentAndQuickSlots ItemStacks MassFarming UsefulTrophies QuickStackStore YamlDotNet

plugins_path="$script_dir/plugins"
tmp_dir="/tmp/plugins_vh"
version_check_path="$plugins_path/.versions"

# exportando variáveis para funcionar ao usar o parallel
export gum
export columns
export width_text
export margin_text
export script_dir
export plugins
export plugins_path
export tmp_dir
export version_check_path

mkdir -p "$tmp_dir" "$version_check_path"

check_dependences() {
	local dist=$(lsb_release -is)
	local need_install=""
	local return=""
	for i in $1; do
		return="$(check_command $i)"
		[[ ! $return = "" ]] && need_install="$return $need_install"
	done
	if [[ ! $need_install = "" ]]; then
		print_alert "❗❗❗ Será necessário instalar estes 👇👇👇👇 pacotes para usar esse script. ❗❗❗"
		print_alert_bold "$need_install"
		if $(ask_thing "Proceder com a instalação das dependências?"); then
			echo ""
			if [ -x "$(command -v apt)" ]; then
				$gum_installing -- sudo apt -y install $need_install
			elif [ -x "$(command -v xbps-install)" ]; then
				$gum_installing -- sudo xbps-install -Suy $need_install
			elif [ -x "$(command -v dnf)" ]; then
				$gum_installing -- sudo dnf -y install $need_install
			elif [ -x "$(command -v yum)" ]; then
				$gum_installing -- sudo yum install -y $need_install
			else print_alert "Eita. 😅 Não consigo instalar! Instale os pacotes e depois volta aqui." && print_alert "Esses: $($gum style --bold $need_install)" && exit 1; fi
		else
			exit 1
		fi
	fi

}

check_command() {
	if ! command -v $1 &>/dev/null; then
		echo "$1"
	fi
}

ask_thing() {
	$gum confirm --affirmative="Agora!" --negative="Jamais!" --selected.background="2" --selected.foreground="0" "$1"
}

print_title() {
	$gum style --foreground "4" --align "center" --margin "0 $margin_text" --padding "0 2" --width $width_text "$1"
}

print_alert() {
	$gum style --foreground "0" --background "1" --align "center" --margin "0 $margin_text" --padding "1 2" --width $width_text "$1"
}

print_alert_bold() {
	$gum style --foreground "0" --background "1" --align "center" --margin "0 $margin_text" --padding "1 2" --width $width_text --bold "$1"
}

print_text() {
	$gum style --foreground "2" --border "rounded" --align "center" --margin "0 $margin_text" --padding "0 2" --width $width_text "$1"
}

print_markdown() {
	$gum format --type=markdown --theme=dracula "$1"
}

choose_menu() {
	$gum choose $1 --cursor.foreground="3" --selected.foreground="3"
}

detect_network_tool() {
	if command -v curl 2>/dev/null >/dev/null; then
		fetch() {
			command curl -s -X GET "$1" -Lo "$2"
		}
	elif command -v wget 2>/dev/null >/dev/null; then
		fetch() {
			command wget -q -O- -X GET "$1" -LO "$2"
		}
	else
		print_text "Nem o curl nem o wget estão disponíveis, nada de plugins novos pra você!"
	fi
}

parse_args() {
	case $1 in
	XPortal)
		git_url="SpikeHimself/XPortal"
		get_release_url "$git_url" "XPortal"
		[[ -f "$version_check_path/$1.txt" ]] && installed_version="$(cat $version_check_path/$1.txt)" || installed_version=0
		dl_file_name="XPortal-v$release_version.zip"
		install_path="$script_dir/"
		zip_file=true
		zip_stuff_to_unzip="plugins/XPortal"
		param_7z="x"
		check_version "XPortal" "$release_version" "$installed_version"
		[[ $? -ne 1 ]] && get_file_url "$git_url" "$release_version" "$dl_file_name" "v" || return 1
		;;
	Jotunn)
		git_url="Valheim-Modding/Jotunn"
		get_release_url "$git_url" "Jotunn"
		[[ -f "$version_check_path/$1.txt" ]] && installed_version="$(cat $version_check_path/$1.txt)" || installed_version=0
		dl_file_name="Jotunn.dll"
		install_path="$plugins_path"
		zip_file=false
		zip_stuff_to_unzip="semzip"
		param_7z="semzip"
		check_version "Jotunn" "$release_version" "$installed_version"
		[[ $? -ne 1 ]] && get_file_url "$git_url" "$release_version" "$dl_file_name" "v" || return 1
		;;
	MassFarming)
		git_url="Xeio/MassFarming"
		get_release_url "$git_url" "MassFarming"
		[[ -f "$version_check_path/$1.txt" ]] && installed_version="$(cat $version_check_path/$1.txt)" || installed_version=0
		dl_file_name="MassFarming.zip"
		install_path="$plugins_path/"
		zip_file=true
		zip_stuff_to_unzip="MassFarming/MassFarming.dll"
		param_7z="e"
		check_version "MassFarming" "$release_version" "$installed_version"
		[[ $? -ne 1 ]] && get_file_url "$git_url" "$release_version" "$dl_file_name" "v" || return 1
		;;
	ValheimPlus_n_usar)
		git_url="valheimPlus/ValheimPlus"
		get_release_url "$git_url" "ValheimPlus"
		[[ -f "$version_check_path/$1.txt" ]] && installed_version="$(cat $version_check_path/$1.txt)" || installed_version=0
		dl_file_name="ValheimPlus.dll"
		install_path="$plugins_path"
		zip_file=false
		zip_stuff_to_unzip="semzip"
		param_7z="semzip"
		check_version "ValheimPlus" "$release_version" "$installed_version"
		[[ $? -ne 1 ]] && get_file_url "$git_url" "$release_version" "$dl_file_name" "" || return 1
		;;
	ValheimPlus)
		git_url="Grantapher/ValheimPlus"
		get_release_url "$git_url" "ValheimPlus"
		[[ -f "$version_check_path/$1.txt" ]] && installed_version="$(cat $version_check_path/$1.txt)" || installed_version=0
		dl_file_name="ValheimPlus.dll"
		install_path="$plugins_path"
		zip_file=false
		zip_stuff_to_unzip="semzip"
		param_7z="semzip"
		check_version "ValheimPlus" "$release_version" "$installed_version"
		[[ $? -ne 1 ]] && get_file_url "$git_url" "$release_version" "$dl_file_name" "" || return 1
		;;
	*)
		print_text "Plugin $1 não configurado!"
		exit 1
		;;
	esac

	if [[ "$zip_file" = true && "$param_7z" = "x" ]]; then
		dest="$script_dir/$zip_stuff_to_unzip"
	elif [[ "$param_7z" = "e" ]]; then
		dest="$plugins_path/$(echo $zip_stuff_to_unzip | awk -F '/' '{print $NF}')"
	else
		dest="$plugins_path/$dl_file_name"
	fi
}

unzip_plugin() {
	if [[ "$1" = true ]]; then
		7z -bso0 $5 $2 $3 -o$4
	else
		rsync -a $2 $4
	fi
}

check_version() {

	if [[ "$2" == "$3" ]]; then
		print_text "A última versão do plugin $1 é $2 e já está instalado."
		return 1
	else
		echo "$2" >$version_check_path/$1.txt
		return 0
	fi

}

get_file_url() {
	url="https://github.com/$1/releases/download/$4$2/$3"
}

get_release_url() {
	release_version="$(curl -X GET https://github.com/$1/releases/latest -L --output - 2>/dev/null | sed -En 's/.*tree\/v?(\S+)".*/\1/p' | head -n1)"
	[[ -z "$release_version" ]] && print_text "Não foi possível pegar a última versão do plugin $2. =("
}

download_plugin() {
	print_text "Baixando do endereço: $url"
	fetch "$url" "$tmp_dir/$dl_file_name"
}

prepare_install_dest() {
	print_text "Instalando em $dest"
	[[ ! -z "$dest" ]] && \rm -Rf "$dest"
}

clean_exports() {
	unset gum columns script_dir plugins plugins_path tmp_dir version_check_path parse_args download_plugin unzip_plugin get_release_url check_version get_file_url print_text detect_network_tool fetch prepare_install_dest
}

main() {
	check_dependences "$dependences"
	detect_network_tool
	export -f parse_args
	export -f download_plugin
	export -f unzip_plugin
	export -f get_release_url
	export -f check_version
	export -f get_file_url
	export -f print_text
	export -f detect_network_tool
	export -f fetch
	export -f prepare_install_dest
	print_title "______           __   __            _          
|  _  \          \ \ / /           (_)          
| | | |__ _ _ __  \ V /  __ ___   ___  ___ _ __ 
| | | / _\` | '_ \ /   \ / _\` \ \ / / |/ _ \ '__|
| |/ / (_| | | | / /^\ \ (_| |\ V /| |  __/ |   
|___/ \__,_|_| |_\/   \/\__,_| \_/ |_|\___|_|   
Dan Xavier - https://danxavier.com.br           

#########################################
## ATUALIZADOR DE PLUGINS PARA VALHEIM ##
#########################################
"

	$gum_loading parallel 'parse_args {} && [[ $? -ne 1 ]] && download_plugin && prepare_install_dest && unzip_plugin "$zip_file" "$tmp_dir/$dl_file_name" "$zip_stuff_to_unzip" "$install_path" "$param_7z" && \rm "$tmp_dir/$dl_file_name"' ::: $plugins
	#	for app in $plugins
	#	do
	#		parse_args "$app"
	#    [[ $? -ne 1 ]] && download_plugin && prepare_install_dest && unzip_plugin "$zip_file" "$tmp_dir/$dl_file_name" "$zip_stuff_to_unzip" "$install_path" "$param_7z" && \rm "$tmp_dir/$dl_file_name"
	##	    	[[ $? -ne 1 ]] && download_plugin && create_link "$dest" "$execpath/$app" && create_desktop "$app" "$execpath"
	#	done
	clean_exports
	exit 0
}

main
